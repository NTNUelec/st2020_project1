const express = require('express')
const path = require('path');
const app = express();


app.set('PORT', process.env.PORT || 3000)

app.use(express.static(path.join(__dirname + '/../public')));
// app.use("/styles", express.static(__dirname + '/../public/css'));
app.use("/scripts", express.static(__dirname + '/../public/bundle.js'));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/../public/index.html'));
});

app.listen(app.get('PORT'), () =>
    console.log(`Server running on port ${app.get('PORT')}`),
)