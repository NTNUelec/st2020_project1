# [Project 1 - Gitlab CI, pipeline & unit test](https://hackmd.io/@xlYUTygoRkyuQQlwXuWDWQ/ByaSzDCNL)


Student ID: r08942066


## Part 1: Unit test (30%)

1. Implement `src/utils/helper.js`.
2. [Bonus] Test lodash, write your test cases in the `tests/lodash.test.js`.

## Part 2: Pipeline (40%)

1. Add a job called `test function` in test stage parallelly.
2. Add cache to speed up pipeline

## Part 3: Deployment (30%)

* Staging: https://st2020-r08942066-staging.herokuapp.com/
* Prodection: https://st2020-r08942066.herokuapp.com/
