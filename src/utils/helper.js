
let sorting = (array) => {
	array.sort();	
    return array;
}

let compare = (a, b) => {
	if (a.SiteName < b.SiteName) {
		return 1;
	}
	if (a.SiteName > b.SiteName) {
		return -1;
  	}
  	// a 必須等於 b
 	return 0;
}

let average = (nums) => {
	const arrAvg = arr => (arr.reduce((a,b) => a + b, 0) / arr.length).toFixed(2)
	var result = parseFloat(arrAvg(nums));

	return result;
}


module.exports = {
    sorting,
    compare,
    average
}